app.controller('counterController', function($scope) {
  $scope.counter = 0;
  $scope.incrementCounter = () => $scope.counter++;
  $scope.decrementCounter = () => $scope.counter--;
});
