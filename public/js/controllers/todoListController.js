app.controller('todoListController', function($scope) {
  $scope.newTodoText = "";
  $scope.todos = [];
  $scope.addTodo = (text) => {
    text = text.trim();
    if (!text) return;
    $scope.todos.push({ text, created: new Date()});
    $scope.newTodoText = "";
    console.log($scope.todos);
  }
  $scope.removeTodo = (index) => $scope.todos.splice(index, 1);
});

app.directive("todoList", () => {
  return {
    link: (scope, element, attrs) => {
      scope.todos = scope[attrs["source"]] || scope["todos"];
    },
    restrict: "E",
    templateUrl: "js/templates/todoListTemplate.html",
  };
});
