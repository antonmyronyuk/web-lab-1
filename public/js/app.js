const app = angular.module('app', []);

app.filter("upperCase", () => (text) => text.toUpperCase());
app.filter("lowerCase", () => (text) => text.toLowerCase());
